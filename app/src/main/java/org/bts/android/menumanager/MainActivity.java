package org.bts.android.menumanager;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements ActionMode.Callback, View.OnLongClickListener, View.OnClickListener, PopupMenu.OnMenuItemClickListener {
    private PopupMenu mPopMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView tvhelo = findViewById(R.id.tv_hello_activity_main);
        this.registerForContextMenu(tvhelo);

        ConstraintLayout long_const = findViewById(R.id.cons_id);
        long_const.setOnLongClickListener(this);

        Button btnLaunchActivity = findViewById(R.id.btn_start);
        btnLaunchActivity.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.icon_user_white) {
            Toast.makeText(this, "user", Toast.LENGTH_SHORT).show();
        } else if (item.getItemId() == R.id.icon_cannabis) {
            // created on long_const (the whole screen) wherever you set the anchor
            mPopMenu = new PopupMenu( this, this.findViewById(R.id.icon_cannabis));
            //Create the pop Up menu
            MenuInflater mMenuInflater = mPopMenu.getMenuInflater();
            mMenuInflater.inflate(R.menu.popup_menu, mPopMenu.getMenu());
            mPopMenu.setOnMenuItemClickListener(this);
            // The pop-up menu is configured to be clickable and later is
            // when click on this icon show where you said before
            mPopMenu.show();
            Toast.makeText(this, "Cut", Toast.LENGTH_SHORT).show();
        } else {
            return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater mContMenuInflater = this.getMenuInflater();
        mContMenuInflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        if (item.getItemId() == R.id.likeItem)   {
            Toast.makeText(this, "LIKE context item selected", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        MenuInflater mContMenuInflater = this.getMenuInflater();
        mContMenuInflater.inflate(R.menu.contextual_menu_actions, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.item_warning_contextual_menu){
            Toast.makeText(this, "warning baby!", Toast.LENGTH_SHORT).show();

        } else if (menuItem.getItemId() == R.id.item_error_contextual_menu){
            Toast.makeText(this, "baby gone bad", Toast.LENGTH_SHORT).show();

        } else {
            return false;
        }
        actionMode.finish();
        return true;
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {

    }

    @Override
    public boolean onLongClick(View view) {
        if (view.getId() == R.id.cons_id) {
            startActionMode(this);

            return true;
        }
        return false;
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        return false;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_start){
            Intent intent = new Intent(this, ActivityTwo.class);
            startActivity(intent);
        }
    }
}
