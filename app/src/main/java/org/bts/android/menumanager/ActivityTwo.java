package org.bts.android.menumanager;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.Toast;


public class ActivityTwo extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two);

        Button btnKillAct = findViewById(R.id.btn_back);
        btnKillAct.setOnClickListener(this);

        Toolbar mToolbar = findViewById(R.id.toolbar);
        this.setSupportActionBar(mToolbar);
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.icon_user_white) {
            Toast.makeText(this, "user profile", Toast.LENGTH_SHORT).show();
        } else if (item.getItemId() == R.id.icon_cannabis) {
            // created on long_const (the whole screen) wherever you set the anchor
            //mPopMenu = new PopupMenu( this, this.findViewById(R.id.item_cut_menu_main));
            //Create the pop Up menu
            //MenuInflater mMenuInflater = mPopMenu.getMenuInflater();
            //mMenuInflater.inflate(R.menu.popup_menu, mPopMenu.getMenu());
            //mPopMenu.setOnMenuItemClickListener(this);
            // The pop-up menu is configured to be clickable and later is
            // when click on this icon show where you said before
            //mPopMenu.show();
            Toast.makeText(this, "weed icon", Toast.LENGTH_SHORT).show();
        } else {
            return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_back){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }
}
